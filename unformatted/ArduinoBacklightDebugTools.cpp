//#include <EEPROM.h>

//boolean buttonState;
//byte eePos = 0;

int incomingByte = 0;
int ledNr = 0;
int nextBit = 0;
int lastByte = 0;
//byte should[4];
//byte currentBrightness[4];
byte leds[4] = {9, 10, 11, 6};
int loopCounter = 0;
//boolean animateChanges = false;

void setup() {
  Serial.begin(9600);
//  pinMode(13, OUTPUT);
  pinMode(2, INPUT);  
}

void loop() {/* 
 if(GetButton())  {
 eeDump(0, 127);
 
 delay(20000);
 }
 else*/

  if(Serial.available() != 0)  {
    incomingByte = Serial.read();
    if(incomingByte != lastByte)  {
      lastByte = incomingByte;
    // eePush(incomingByte);       

    boolean isLedNr = (incomingByte & 128) == 128;
    if(isLedNr)  {
      ledNr = incomingByte & 63;
      nextBit = (incomingByte & 64) << 1;
  //    animateChanges = false;
    }
    else  {  
      int fadeValue = nextBit + incomingByte;
      // eePush(1000);
      // eePush(ledNr);
      // eePush(fadeValue);             
//      should[ledNr] = fadeValue;
  //    animateChanges = true;
      analogWrite(leds[ledNr], fadeValue);
      //        if(ledNr >= 9 && ledNr <= 12)  {
      //         if(ledNr == 12) ledNr = 6;
      //        analogWrite(ledNr, fadeValue);
      //     }  
    }
    
    Serial.print("!");
  }
  }
  /*else if(animateChanges)  {
    if(loopCounter++ == 12000)  {
      loopCounter = 0;
      boolean checkChanges = false; 
      for(int i = 0; i < 4 == 0 && Serial.available() == 0; i++)  {
        byte currentShould = should[i];
        byte actual = currentBrightness[i];
        if(actual != currentShould)  {
          checkChanges = true;
          if(actual < currentShould) actual++;
          else actual--;
          analogWrite(leds[i], actual);
          currentBrightness[i] = actual;
        }
      }
      if(checkChanges == false) animateChanges = false;
    }
  }*/
}

/*
boolean GetButton()  {
 boolean newButtonState = digitalRead(2);
 boolean buttonChanged = newButtonState != buttonState;
 buttonState = newButtonState; 
 if(buttonChanged) digitalWrite(13, newButtonState);
 return buttonState;    
 }
 
 void Init()  {
 eeInit(); 
 InitSignal();
 }
 
 void InitSignal()  {
 analogWrite(9, 255); 
 delay(1000); 
 analogWrite(9, 0); 
 }
 
 void errSignal()  {
 while(true)  {
 analogWrite(9, 255); 
 delay(1000); 
 analogWrite(9, 0);
 delay(1000); 
 analogWrite(9, 127); 
 delay(1000); 
 analogWrite(9, 0); 
 delay(1000); 
 }  
 }
 
 void eeInit()  {
 eePos = EEPROM.read(0);
 eePush(255);
 eePush(255);
 eePush(255);
 eePush(255);  
 }
 
 void eePush(int value)  {
 eePos = eePos + 1;
 if(eePos >= 127) eePos = 1;
 EEPROM.write(0, eePos);
 eeSet(eePos, value);
 if(eeGet(eePos) != value) errSignal();
 }
 
 int eeGetRealPos(int pos)  {
 return (pos +1) << 2; 
 }
 
 int eeGet(int pos)  {
 int realPos = eeGetRealPos(pos);
 //dumpInfo("get ", pos);
 //dumpKeyValue(realPos -3, EEPROM.read(realPos -3));
 //dumpKeyValue(realPos -2, EEPROM.read(realPos -2) << 8);
 //dumpKeyValue(realPos -1, EEPROM.read(realPos -1) << 16);
 //dumpKeyValue(realPos, EEPROM.read(realPos) << 24);
 //Serial.println("Done");
 
 return EEPROM.read(realPos -3) + (EEPROM.read(realPos -2) << 8) + (EEPROM.read(realPos -1) << 16) + (EEPROM.read(realPos) << 24);
 }
 
 void eeSet(int pos, int value)  {
 int realPos = eeGetRealPos(pos);
 //dumpInfo("set ", pos);
 //dumpInfo("to ", value);
 //dumpKeyValue(realPos -3, value & 255);
 //dumpKeyValue(realPos -2, (value >> 8)  & 255);
 //dumpKeyValue(realPos -1, (value >> 16)  & 255);
 //dumpKeyValue(realPos, value >> 24);
 //Serial.println("Done");
 
 EEPROM.write(realPos -3, value & 255);
 EEPROM.write(realPos -2, (value >> 8) & 255);
 
 EEPROM.write(realPos -1, (value >> 16) & 255);
 EEPROM.write(realPos, value >> 24);
 }
 
 void dumpInfo(String key, int value)  {
 Serial.print(key);
 Serial.println(value, DEC);
 }
 
 void dumpKeyValue(int key, int value)  {
 Serial.print(key, DEC);
 Serial.print(" => ");
 Serial.println(value, DEC);
 }
 
 void eeDump(int from, int to)  {
 int i;
 for(i = from; i < to; i++)  {
 dumpKeyValue(i, eeGet(i));
 }
 }
 
 
 
 */


